import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { setLanguage, getLanguage } from 'react-multi-lang'

import { Select, MenuItem } from '@material-ui/core'
import InputBase from '@material-ui/core/InputBase'

import Title from './Title'

import SunIcon from '@material-ui/icons/WbSunny'
import MoonIcon from '@material-ui/icons/Brightness3'

import FrIcon from '../../Assets/Flag/france.svg'
import EnIcon from '../../Assets/Flag/united-kingdom.svg'

const Flags = [
  {
    icon: FrIcon,
    code: 'fr'
  },
  {
    icon: EnIcon,
    code: 'en'
  },
]


const CustomInput = withStyles((theme) => ({
  root: {
    marginLeft: 'auto',
    marginRight: 0,
    paddingTop: 5,
  },
  input: {
    border: 'none',
    backgroundColor: 'transparent',
    position: 'relative',
    paddingRight: '15px !important',
  }
}))(InputBase)

const styles = (theme) => ({
  header: {
    width: '-webkit-fill-available',
    height: 50,
    paddingBottom: 10,
    paddingTop: 10,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: theme.custom.headerBackground,
    display: 'flex',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 10,
    borderBottom: theme.custom.borderContainer
  },
  themeButton: {
    width: 40,
    height: 40,
    marginRight: 0,
    padding: 0,
    border: 'none',
    outline: 'none',
    color: theme.custom.headerButton,
    backgroundColor: 'transparent',
    textAlign: 'center',
    boxSizing: 'border-box',
    cursor: 'pointer'
  },
  themeIcon: {
    width: 30,
    height: 30,
    marginTop: 5,
  },
  langIcon: {
    width: 30,
    height: 30,
  },
  arrow: {
    display: 'none',
  },
  divider: {
    width: 1,
    height: 'inherit',
    backgroundColor: theme.custom.headerButton,
    padding: '3px 0'
  }
})

class Header extends Component {
  state = {
    lang: 'en',
  }

  componentDidMount() {
    this.setState({ lang: getLanguage() })
  }

  setLanguage = (lang) => {
    console.log(lang)
    this.setState({ lang }, () => {
      const { lang } = this.state
      setLanguage(lang)
    })
  }

  render() {
    const { classes, onChangeTheme, darkMode } = this.props
    const { lang } = this.state

    return (
      <div className={classes.header}>
        <Title />
        <Select
          value={lang}
          onChange={(event) => this.setLanguage(event.target.value)}
          input={<CustomInput />}
          inputProps={{ classes: { icon: classes.arrow } }}
        >
          {
            Flags.map((e, i) => {
              return <MenuItem value={e.code} key={i}><img className={classes.langIcon} src={e.icon} alt="flag" /></MenuItem>
            })
          }
        </Select>
        <div className={classes.divider} />
        <button className={classes.themeButton} onClick={onChangeTheme}>
          {
            ( darkMode && <MoonIcon className={classes.themeIcon} /> )
            || <SunIcon className={classes.themeIcon} />
          }
        </button>
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  onChangeTheme: PropTypes.func.isRequired,
  darkMode: PropTypes.bool.isRequired,
}

Header.defaultProps = { }

export default withStyles(styles)(Header);