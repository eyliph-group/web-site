import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import Logo from '../../Assets/Header/title_href.svg'

const styles = {
  title: {
    width: 'fit-content',
    height: 'inherit',
  },
  titleButton: {
    height: 'inherit',
    backgroundColor: 'transparent',
    border: 'none',
    margin: 0,
    padding: 0,
    '&:focus, &:hover' : {
      cursor: 'pointer',
      outline: 'none',
    }
  },
  titleImage: {
    width: 'inherit',
    height: 'inherit',
  }
}

class Header extends Component {

  onClick = () => {
    const { history } = this.props

    history.push('/')
  }

  render() {
    const { classes } = this.props

    return (
      <div className={classes.title}>
        <button className={classes.titleButton} onClick={this.onClick}>
          <img className={classes.titleImage} src={Logo} alt="logo" />
        </button>
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
}

Header.defaultProps = { }

export default withStyles(styles)(withRouter(Header));