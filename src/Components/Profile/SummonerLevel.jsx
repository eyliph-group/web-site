import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import './SummonerLevel.css'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: 'fit-content',
    marginTop: props => props.summonerLevel.toString().length === 4 ? -22 : -20
  },
  container: {
    margin: 'auto',
    width: 'fit-content',
    padding: '0 3px',
    height: 25,
    textAlign: 'center',
    marginTop: 1,
    borderTop: '1px solid #eabd70',
    borderBottom: '1px solid #eabd70'
  },
  text: {
    color: '#f5c590',
    fontWeight: 600,
  }
})

class SummonerLevel extends Component {
  render() {
    const {
      classes,
      summonerLevel
    } = this.props

    return (
      <div className={classes.main}>
        <div className={classNames({ [classes.container]: true, 'container': true })}>
          <span className={classes.text}>
            {summonerLevel}
          </span>
        </div>
      </div>
    );
  }
}

SummonerLevel.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  summonerLevel: PropTypes.number.isRequired,
}

SummonerLevel.defaultProps = { }

export default withStyles(styles)(SummonerLevel);