import React, { Component } from 'react'
import { withStyles, withTheme } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { withTranslation  } from 'react-multi-lang'

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import SelectInput from './SelectInput'

import Region from '../../json/data/region.json'

const styles = (theme) => ({
  inputContainer: {
    margin: 0,
    display: 'flex',
    padding: 0,
    height: 36,
    fontSize: '16px',
    fontWeight: 400,
  },
  inputField: {
    marginLeft: -2,
    outline: 'none',
    boxSizing: 'border-box',
    paddingLeft: 10,
    color: theme.custom.search.textColor,
    backgroundColor: theme.custom.search.inputBackground,
    width: '100%'
  },
  searchButton: {
    marginRight: 'auto',
    marginLeft: 0,
    padding: 0,
    outline: 'none',
    border: 'none',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    minWidth: 72,
    color: theme.custom.search.button.text,
    backgroundColor: theme.custom.search.button.default,
    cursor: 'pointer',
    "&:disabled": {
      backgroundColor: theme.custom.search.button.disabled,
      opacity: 0.5,
      cursor: "not-allowed"
    }
  }
})

class SearchBox extends Component {
  state = {
    inputFocused: false,
    region: 'EUW1',
    summonerName: "",
  }

  handleKeyDown = (e) => {
    const { summonerName } = this.state

    if (e.key === 'Enter' && !(!summonerName.match(/[^\s]/gi) || summonerName === "")) {
      this.onButtonPressed()
    }
  }

  onButtonPressed = () => {
    const { onSearch } = this.props
    const { region, summonerName } = this.state

    onSearch(region, summonerName)
  }

  render() {
    const { classes, t, theme } = this.props
    const {
      inputFocused,
      region,
      summonerName
    } = this.state

    return (
        <div className={classes.inputContainer}>
          <Select
            value={region}
            onChange={(event) => { this.setState({ region: event.target.value }) }}
            input={<SelectInput inputfocused={inputFocused.toString()} />}
          >
            {
              Region.map((e, i) => {
                return <MenuItem value={e.platform} key={i}>{e.name}</MenuItem>
              })
            }
          </Select>
          <input
            type="text"
            className={classes.inputField} style={{ border: inputFocused ? theme.custom.search.blueInputBorder : theme.custom.search.inputBorder }}
            value={summonerName}
            onChange={(event) => this.setState({ summonerName: event.target.value })}
            onFocus={() => this.setState({ inputFocused: true })}
            onBlur={() => this.setState({ inputFocused: false })}
            onKeyDown={this.handleKeyDown}
          />
          <button className={classes.searchButton} onClick={this.onButtonPressed} disabled={!summonerName.match(/[^\s]/gi) || summonerName === ""}>
            {t('global.search')}
          </button>
        </div>
    );
  }
}

SearchBox.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  t: PropTypes.instanceOf(Object).isRequired,
  theme: PropTypes.instanceOf(Object).isRequired,
  onSearch: PropTypes.func.isRequired,
}

SearchBox.defaultProps = { }

export default withStyles(styles)(withTheme(withTranslation(SearchBox)));