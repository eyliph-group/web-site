import { withStyles } from '@material-ui/core/styles'
import InputBase from '@material-ui/core/InputBase';

const styles = (theme) => ({
  root: {
    marginLeft: 'auto'
  },
  input: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    border: props => props.inputfocused === 'true' ? theme.custom.search.blueInputBorder : theme.custom.search.inputBorder,
    width: 72,
    height: 36,
    backgroundColor: theme.custom.search.inputBackground,
    boxSizing: 'border-box',
    fontSize: 15,
    padding: '8px 26px 8px 10px',
    color: theme.custom.search.textColor,
    '&:focus': {
      borderTopLeftRadius: 10,
      borderBottomLeftRadius: 10,
      border: props => props.inputfocused === 'true' ? theme.custom.search.blueInputBorder : theme.custom.search.inputBorder,
      backgroundColor: theme.custom.search.inputBackground,
    }
  }
})

export default withStyles(styles)(InputBase)