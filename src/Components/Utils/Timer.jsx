import React, { useState, useEffect } from 'react'
import { withStyles, withTheme } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const styles = (theme) => ({
  textTimer: {
    backgroundColor: 'transparent',
    color: 'white',
  }
})

function Timer({ classes, initialTime }) {
  const [mSeconds, setMSeconds] = useState(Math.abs(initialTime - new Date()))
  const [isActive, setIsActive] = useState(false)

  useEffect(() => {
    const interval = null
    if (!isActive) {
      setInterval(() => {
        setMSeconds(mSeconds => mSeconds + 1000)
      }, 1000)
      setIsActive(true)
    }
    return () => clearInterval(interval)
  }, [mSeconds, isActive])

  useEffect(() => {
    setMSeconds(Math.abs(initialTime - new Date()))
  }, [initialTime])

  return (
    <div className={classes.textTimer}>
      {Math.trunc(mSeconds / 60000)}:{`${Math.trunc((mSeconds / 1000) % 60) < 10 ? 0 : ''}${Math.trunc(mSeconds / 1000) % 60}`}
    </div>
  );
}

Timer.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  initialTime: PropTypes.number.isRequired,
}

Timer.defaultProps = { }

export default withStyles(styles)(withTheme(Timer));