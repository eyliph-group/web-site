import React from 'react'
import { Config } from '../../js/config'

const Loading = () => {
  return <img src={`${Config.ddragonEndpoint}/img/global/load01.gif`} alt="load" />
}

export default Loading