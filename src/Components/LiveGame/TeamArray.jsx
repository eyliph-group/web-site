import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { Grid } from '@material-ui/core'

import HeadBar from './TeamHeadBar'
import PlayerGameInfo from './PlayerGameInfo'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '100%',
    position: 'relative'
  },
})

class TeamArray extends Component {
  render() {
    const { classes, participants, bannedChampions, region, level, queueId, blue, red } = this.props

    return (
      <div className={classes.main}>
        <Grid container direction='column' justify='flex-start' alignItems='stretch'>
          <Grid item xs={12}>
            <HeadBar blue={blue} red={red} />
          </Grid>
          { participants.map((e, i) =>
            e.teamId === (red ? 200 : 100) &&
            <Grid item xs={12} key={i} >
              <PlayerGameInfo player={e} region={region} level={level} queueId={queueId} bannedChampion={bannedChampions.length && bannedChampions.find(e => e.pickTurn - 1 === i).championId} />
            </Grid>
          )}
        </Grid>
      </div>
    );
  }
}


TeamArray.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  participants: PropTypes.instanceOf(Object).isRequired,
  bannedChampions: PropTypes.instanceOf(Array).isRequired,
  region: PropTypes.string.isRequired,
  queueId: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
  blue: PropTypes.bool,
  red: PropTypes.bool
}

TeamArray.defaultProps = {
  blue: false,
  red: false
}

export default withStyles(styles)(TeamArray);