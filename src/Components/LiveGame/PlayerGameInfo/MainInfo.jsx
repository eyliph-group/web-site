import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import { Config } from '../../../js/config'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '25px',
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
  },
  champion: {
    height: '25px',
    margin: 'auto 5px auto 30px'
  },
  spellcontainer: {
    height: 25,
    display: 'grid',
    marginRight: 10
  },
  spell: {
    height: 10,
    width: 10,
    border: '1px solid black',
    margin: 0
  },
  perkContainer: {
    alignItems: 'center',
    height: '25px',
    display: 'flex'
  },
  mainPerkContainer: {
    height: 18,
    width: 18,
    backgroundColor: 'black',
    borderRadius: '50%',
  },
  mainPerk: {
    height: 18,
  },
  subPerk: {
    height: 10,
    margin: '10px 0 0 -5px'
  },
  nameContainer: {
    alignItems: 'center',
    height: '25px',
    display: 'flex',
    margin: '0 0 0 10px'
  },
  summonerNameText: {
    fontWeight: 500,
    fontSize: 12,
    color: theme.custom.liveGame.textColor,
  }
})

class PlayerGameInfo extends Component {
  render() {
    const {
      classes,
      championId,
      spell1Id,
      spell2Id,
      perkStyle,
      perkSubStyle,
      mainPerk,
      summonerName
    } = this.props

    const spell1 = Object.values(Config.summSpells.data).find(e => e.key === spell1Id.toString()).id
    const spell2 = Object.values(Config.summSpells.data).find(e => e.key === spell2Id.toString()).id
    const championIcon = Object.values(Config.championsInfo.data).find(e => e.key === championId.toString()).id
    const perkSubStyleIcon = Config.runesReforged.find(e => e.id === perkSubStyle).icon
    const mainPerkIcon =
      Config.runesReforged.find(e => e.id === perkStyle)
      .slots[0].runes.find(e => e.id === mainPerk).icon

    return (
      <div className={classes.main}>
        <img className={classes.champion} src={`${Config.ddragonEndpoint}/${Config.ddragonApiVersion}/img/champion/${championIcon}.png`} alt='Champion icon' />
        <div className={classes.spellcontainer}>
          <div
            className={classes.spell}
            style={{ background: `center / contain url(${Config.ddragonEndpoint}/${Config.ddragonApiVersion}/img/spell/${spell1}.png), black` }}
          />
          <div
            className={classes.spell}
            style={{ background: `center / contain url(${Config.ddragonEndpoint}/${Config.ddragonApiVersion}/img/spell/${spell2}.png), black` }}
          />
        </div>
        <div className={classes.perkContainer}>
          <div className={classes.mainPerkContainer}>
            <img className={classes.mainPerk} src={`${Config.ddragonEndpoint}/img/${mainPerkIcon}`} alt='Main style' />
          </div>
          <img className={classes.subPerk} src={`${Config.ddragonEndpoint}/img/${perkSubStyleIcon}`} alt='Main sub style' />
        </div>
        <div className={classes.nameContainer}>
          <span className={classes.summonerNameText}>
            {summonerName}
          </span>
        </div>
      </div>
    );
  }
}


PlayerGameInfo.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  championId: PropTypes.number.isRequired,
  spell1Id: PropTypes.number.isRequired,
  spell2Id: PropTypes.number.isRequired,
  perkStyle: PropTypes.number.isRequired,
  perkSubStyle: PropTypes.number.isRequired,
  mainPerk: PropTypes.number.isRequired,
  summonerName: PropTypes.string.isRequired
}

PlayerGameInfo.defaultProps = { }

export default withStyles(styles)(PlayerGameInfo);