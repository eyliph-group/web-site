import React, { Component } from 'react'
import { withStyles, withTheme } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import { Config } from '../../../js/config'

const styles = (theme) => ({
  main: {
    display: 'flex',
    position: 'relative',
  },
  champion: {
    width: 25,
    margin: 'auto'
  }
})

class Ban extends Component {
  render() {
    const { classes, bannedChampion } = this.props
    const championIcon = (!bannedChampion || bannedChampion === -1) ? undefined : Object.values(Config.championsInfo.data).find(e => e.key === `${bannedChampion}`).id

    return (
      <div className={classes.main}>
        { championIcon &&
          <img className={classes.champion} src={`${Config.ddragonEndpoint}/${Config.ddragonApiVersion}/img/champion/${championIcon}.png`} alt='icon' />
        }
      </div>
    );
  }
}


Ban.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  bannedChampion: PropTypes.number.isRequired,
}

Ban.defaultProps = { }

export default withStyles(styles)(withTheme(Ban));