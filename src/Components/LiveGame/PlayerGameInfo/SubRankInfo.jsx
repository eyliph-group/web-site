import React, { Component, Fragment } from 'react'
import { withStyles, withTheme } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '25px',
    position: 'relative',
    display: 'grid',
    alignItems: 'center',
  },
  playedText: {
    fontWeight: 400,
    fontSize: 11,
    color: theme.custom.liveGame.triColor,
  },
  outlineBar: {
    backgroundColor: theme.custom.liveGame.subColor,
    width: 100,
    height: 6,
    border: theme.custom.borderContainer,
    margin: '0 auto',
  },
  filledBar: {
    height: 'inherit',
    border: 'none',
  }
})

class RankInfo extends Component {
  parseRankInfo = (rankData) => {
    const { queueId } = this.props
    let watchedQueue = null

    if (queueId === 440)
      watchedQueue = rankData.find(e => e.queueType === "RANKED_FLEX_SR")
    if (!watchedQueue)
      watchedQueue = rankData.find(e => e.queueType === "RANKED_SOLO_5x5")
    return watchedQueue || { tier: 'UNRANKED' }
  }

  render() {
    const { classes, theme, rankData } = this.props
    const { tier, wins, losses } = this.parseRankInfo(rankData.data)
    const percent = Math.round(wins / (losses + wins) * 100)
    const percentColor = percent >= 70 ? theme.custom.liveGame.orange : (percent >= 50 ? theme.custom.liveGame.green : theme.custom.liveGame.triColor)

    return (
      <div className={classes.main}>
        { tier !== 'UNRANKED' &&
          <Fragment>
            <span className={classes.playedText}><b style={{ color: percentColor}}>{percent}%</b> ({wins + losses} played)</span>
            <div className={classes.outlineBar}>
              <div className={classes.filledBar} style={{ backgroundColor: percentColor, width: `${percent}%` }} />
            </div>
          </Fragment>
        }
        { tier === 'UNRANKED' &&
          <div>
            <span className={classes.playedText}>-</span>
          </div>
        }
      </div>
    );
  }
}


RankInfo.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  rankData: PropTypes.instanceOf(Object).isRequired,
  queueId: PropTypes.number.isRequired,
}

RankInfo.defaultProps = { }

export default withStyles(styles)(withTheme(RankInfo));