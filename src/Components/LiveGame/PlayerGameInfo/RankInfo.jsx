import React, { Component, Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import Rank from '../../../js/rank'

import CheckIcon from '@material-ui/icons/CheckCircleRounded'
import CancelIcon from '@material-ui/icons/Cancel'
import RemoveIcon from '@material-ui/icons/Remove'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '25px',
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
  },
  rankIcon: {
    height: 25,
    borderRadius: '50%',
    margin: 'auto 10px auto 35px',
  },
  rankTextContainer: {
    marginLeft: 0,
    height: 25,
    textAlign: 'left'
  },
  rankText: {
    fontWeight: 500,
    fontSize: 10,
    color: theme.custom.liveGame.textColor,
  }
})

function BestOfComponent({ bestOf }) {
  return (
    <div style={{ display: 'flex', height: 15 }}>
      { bestOf.split('').map((e, i) => {
        switch (e) {
          case 'W':
            return <CheckIcon key={i} style={{ height: 'inherit', width: 20, color: '#1f8ecd' }} />
          case 'L':
            return <CancelIcon key={i} style={{ height: 'inherit', width: 20, color: '#ee5a52' }} />
          default:
            return <RemoveIcon key={i} style={{ height: 'inherit', width: 20, color: '#404040' }} />
        }
      })}
    </div>
  )
}

class RankInfo extends Component {
  parseRankInfo = (rankData) => {
    const { queueId } = this.props
    let watchedQueue = null

    if (queueId === 440)
      watchedQueue = rankData.find(e => e.queueType === "RANKED_FLEX_SR")
    if (!watchedQueue)
      watchedQueue = rankData.find(e => e.queueType === "RANKED_SOLO_5x5")
    return watchedQueue || { tier: 'UNRANKED' }
  }

  render() {
    const { classes, rankData, /* level */ } = this.props
    const watchedQueue = this.parseRankInfo(rankData.data)

    return (
      <div className={classes.main}>
        { !rankData.status &&
          <Fragment>
            <img className={classes.rankIcon} src={Rank.getRankComponentByID(watchedQueue.tier)} alt="Ranked icon" />
            <div className={classes.rankTextContainer}>
              { watchedQueue.tier === 'UNRANKED' &&
                <span className={classes.rankText}>
                  {watchedQueue.tier[0] + watchedQueue.tier.slice(1).toLowerCase()}
                </span>
              }
              { watchedQueue.tier !== 'UNRANKED' &&
                <Fragment>
                  <span className={classes.rankText} style={{ marginTop: watchedQueue.miniSeries && -5 }}>
                    {watchedQueue.tier[0] + watchedQueue.tier.slice(1).toLowerCase()} {watchedQueue.rank} ({watchedQueue.leaguePoints} LP)
                  </span>
                  { watchedQueue.miniSeries &&
                    <BestOfComponent bestOf={watchedQueue.miniSeries.progress} />
                  }
                </Fragment>
              }
            </div>
          </Fragment>
        }
        { rankData.status &&
          <div>
            {JSON.stringify(rankData.status)}
          </div>
        }
      </div>
    );
  }
}


RankInfo.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  rankData: PropTypes.instanceOf(Object).isRequired,
  queueId: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
}

RankInfo.defaultProps = { }

export default withStyles(styles)(RankInfo);