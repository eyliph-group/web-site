import React, { Component } from 'react'
import { withStyles, withTheme } from '@material-ui/core/styles'
import { withTranslation } from 'react-multi-lang'
import PropTypes from 'prop-types'

import Dialog from '@material-ui/core/Dialog'
import Grid from '@material-ui/core/Grid'

import { Config } from '../../../js/config'
import PerksIcon from '../../../json/data/thirdPerksIcon.json'

const styles = (theme) => ({
  main: {
    display: 'flex',
    position: 'relative',
    height: '25px',
  },
  button: {
    margin: 'auto',
    border: theme.custom.borderContainer,
    backgroundColor: theme.custom.bodyBackground,
    height: '100%',
    width: '80%',
    borderRadius: '3px',
  },
  buttonText: {
    color: theme.custom.liveGame.textColor
  },
  container: {
    width: 450,
    margin: '15px',
  },
  divider: {
    width: '1px',
    height: '200px',
    backgroundColor: '#ddd',
    margin: '0 10px'
  }
})

function MainPerks({ id, array }) {
  const perkBranch = Config.runesReforged.find(e => e.id === id)

  return (
    <div>
      <div style={{ display: 'flex' }}>
        <img src={`${Config.ddragonEndpoint}/img/${perkBranch.icon}`} alt="Main style" style={{ margin: 'auto', width: 30 }} />
      </div>
      { perkBranch.slots.map(({ runes }, i) =>
        <div style={{ display: 'flex', margin: '10px 0' }} key={i}>
          { runes.map((e, i) =>
              <img key={i} src={`${Config.ddragonEndpoint}/img/${e.icon}`} alt="Main style" style={{ margin: 'auto', width: 30, filter: array.find(runes => runes === e.id) ? 'none' : 'grayscale(1)' }} />
          )}
        </div>
      )}
    </div>
  )
}

function SubPerks({ id, array }) {
  const perkBranch = Config.runesReforged.find(e => e.id === id)

  return (
    <div>
      <div style={{ display: 'flex' }}>
        <img src={`${Config.ddragonEndpoint}/img/${perkBranch.icon}`} alt="Sub style" style={{ margin: 'auto', width: 30 }} />
      </div>
      { perkBranch.slots.map(({ runes }, i) =>
        i !== 0 &&
        <div style={{ display: 'flex', margin: '10px 0' }} key={i}>
          { runes.map((e, i) =>
              <img key={i} src={`${Config.ddragonEndpoint}/img/${e.icon}`} alt="Sub style" style={{ margin: 'auto', width: 30, filter: array.find(runes => runes === e.id) ? 'none' : 'grayscale(1)' }} />
          )}
        </div>
      )}
    </div>
  )
}

const ThirdPerksOrder = [
  { runes: [5008, 5005, 5007] },
  { runes: [5008, 5002, 5003] },
  { runes: [5001, 5002, 5003] },
]

function ThirdPerks({ array }) {
  return (
    <div>
      { ThirdPerksOrder.map(({ runes }, i) =>
        <div style={{ display: 'flex', margin: '5px 0' }} key={i}>
          { runes.map((id, i) =>
              <img key={i} src={`${Config.ddragonEndpoint}/img/${PerksIcon.find(e => e.id === id).icon}`} alt="Third style" style={{ margin: 'auto', width: 25, filter: array.find(runes => runes === id) ? 'none' : 'grayscale(1)' }} />
          )}
        </div>
      )}
    </div>
  )
}

class Perks extends Component {
  state = { open: false }

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  render() {
    const { classes, t, perks } = this.props
    const { open } = this.state

    return (
      <div className={classes.main}>
        <button onClick={this.handleOpen} className={classes.button}>
          <div className={classes.buttonText}>
            { t('liveGame.runes') }
          </div>
        </button>
        <Dialog open={open} onClose={this.handleClose}>
          <Grid container className={classes.container} alignItems="flex-end">
            <Grid item xs={4}>
              <MainPerks id={perks.perkStyle} array={perks.perkIds} />
            </Grid>
            <div className={classes.divider} />
            <Grid item xs={4}>
              <SubPerks id={perks.perkSubStyle} array={perks.perkIds} />
            </Grid>
            <div className={classes.divider} />
            <Grid item xs style={{ width: '100%' }}>
              <ThirdPerks array={perks.perkIds} />
            </Grid>
          </Grid>
        </Dialog>
      </div>
    );
  }
}

Perks.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  perks: PropTypes.instanceOf(Object).isRequired,
}

Perks.defaultProps = { }

export default withStyles(styles)(withTheme(withTranslation(Perks)));