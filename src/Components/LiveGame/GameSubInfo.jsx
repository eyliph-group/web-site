import React, { Component } from 'react'
import { withStyles, withTheme } from '@material-ui/core/styles'
import { withTranslation } from 'react-multi-lang'
import PropTypes from 'prop-types'

import Timer from '../Utils/Timer'

// import QueueID from '../../json/data/Riot_queueId.json'

const styles = (theme) => ({
  main: {
    display: 'flex',
    position: 'relative',
    margin: '3px auto 3px 10px'
  },
  textTimer: {
    color: theme.custom.liveGame.textColor,
    fontWeight: 400,
    display: 'flex',
    fontSize: 14
  }
})

class GameSubInfo extends Component {
  render() {
    const { classes, t, gameStartTime } = this.props

    return (
      <div className={classes.main}>
        <div className={classes.textTimer}>
          {t('liveGame.timer')}
          &nbsp;
          <Timer initialTime={gameStartTime} classes={ { textTimer: classes.textTimer } } />
        </div>
      </div>
    );
  }
}

GameSubInfo.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  queueId: PropTypes.number.isRequired,
  gameStartTime: PropTypes.number.isRequired,
}

GameSubInfo.defaultProps = { }

export default withStyles(styles)(withTheme(withTranslation(GameSubInfo)));