import React, { Component } from 'react'
import { unstable_createResource } from 'react-cache'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import { Grid } from '@material-ui/core'
import MainInfo from  './PlayerGameInfo/MainInfo'
import RankInfo from './PlayerGameInfo/RankInfo'
import SubRankInfo from './PlayerGameInfo/SubRankInfo'
import Perks from  './PlayerGameInfo/Perks'
import Ban from  './PlayerGameInfo/Ban'

import Api from '../../js/api/endPoint'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '25px',
    position: 'relative',
    margin: '5px 0'
  },
})

const summonerRankResource = unstable_createResource(Api.fetchRankDataByID)

class PlayerGameInfo extends Component {
  render() {
    const { classes, player, region, queueId, level, bannedChampion } = this.props
    const rankData = summonerRankResource.read(JSON.stringify({region, id: player.summonerId}))

    return (
      <Grid container direction='row' className={classes.main}>
        <Grid item xs={3}>
          <MainInfo
            championId={player.championId}
            spell1Id={player.spell1Id}
            spell2Id={player.spell2Id}
            perkStyle={player.perks.perkStyle}
            perkSubStyle={player.perks.perkSubStyle}
            mainPerk={player.perks.perkIds[0]}
            summonerName={player.summonerName}
          />
        </Grid>
        <Grid item xs={2}>
          <RankInfo rankData={rankData} queueId={queueId} level={level} />
        </Grid>
        <Grid item xs={1}>
          <SubRankInfo rankData={rankData} queueId={queueId} />
        </Grid>
        <Grid item xs={2}>
        </Grid>
        <Grid item xs={1}>
        </Grid>
        <Grid item xs={2}>
          <Perks perks={player.perks} />
        </Grid>
        <Grid item xs={1}>
          <Ban bannedChampion={bannedChampion} />
        </Grid>
      </Grid>
    );
  }
}


PlayerGameInfo.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  player: PropTypes.instanceOf(Object).isRequired,
  region: PropTypes.string.isRequired,
  queueId: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
  bannedChampion: PropTypes.number.isRequired,
}

PlayerGameInfo.defaultProps = { }

export default withStyles(styles)(PlayerGameInfo);