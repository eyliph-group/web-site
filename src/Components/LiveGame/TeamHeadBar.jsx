import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { withTranslation } from 'react-multi-lang'
import PropTypes from 'prop-types'

import { Grid } from '@material-ui/core'

import TeamTitle from './TeamTitle'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '40px',
    position: 'relative',
    backgroundColor: theme.custom.bodyBackground,
    borderTop: theme.custom.borderContainer,
    borderBottom: theme.custom.borderContainer,
    marginTop: '5px'
  },
  itemGrid: {
    height: 'inherit',
    display: 'flex',
  },
  basicText: {
    color: props => props.blue ? theme.custom.liveGame.blueTeam : theme.custom.liveGame.redTeam,
    fontWeight: 500,
    fontSize: 12,
    margin: 'auto',
  }
})

class TeamHeadBar extends Component {
  render() {
    const { classes, t, blue, red } = this.props

    return (
      <Grid container direction='row' justify='flex-start' className={classes.main}>
        <Grid item xs={3} className={classes.itemGrid}>
          <TeamTitle blue={blue} red={red} />
        </Grid>
        <Grid item xs={2} className={classes.itemGrid}>
          <div className={classes.basicText}>
            { t('liveGame.rank', { nb: 10 }) }
          </div>
        </Grid>
        <Grid item xs={1} className={classes.itemGrid}>
          <div className={classes.basicText}>
          { t('liveGame.stat', { nb: 10 }) }
          </div>
        </Grid>
        <Grid item xs={2} className={classes.itemGrid}>
          <div className={classes.basicText}>
            { t('liveGame.champStat', { nb: 10 }) }
          </div>
        </Grid>
        <Grid item xs={1} className={classes.itemGrid}>
          <div className={classes.basicText}>
            { t('liveGame.previousRank', { nb: 9 }) }
          </div>
        </Grid>
        <Grid item xs={2} className={classes.itemGrid}>
          <div className={classes.basicText}>
            { t('liveGame.runes') }
          </div>
        </Grid>
        <Grid item xs={1} className={classes.itemGrid}>
          <div className={classes.basicText}>
            { t('liveGame.ban') }
          </div>
        </Grid>
      </Grid>
    );
  }
}


TeamHeadBar.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  blue: PropTypes.bool,
  red: PropTypes.bool
}

TeamHeadBar.defaultProps = {
  blue: false,
  red: false
}

export default withStyles(styles)(withTranslation(TeamHeadBar));