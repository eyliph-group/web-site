import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import { withTranslation } from 'react-multi-lang'

const styles = (theme) => ({
  teamTitle: {
    display: 'flex',
    width: '100%',
    height: '100%',
    position: 'relative',
  },
  teamColorBar: {
    width: '90%',
    height: '100%',
    background: props => `linear-gradient(90deg, ${props.blue ? theme.custom.liveGame.blueTeam : theme.custom.liveGame.redTeam}, 60%, transparent)`,
    border: 'none',
    position: 'absolute',
  },
  teamTitleText: {
    color: theme.custom.liveGame.textColor,
    zIndex: 1,
    fontWeight: 700,
    fontSize: 12,
    margin: 'auto 30px',
  }
})

const TeamTitle = withStyles(styles)((props) => {
  const { classes, t, blue } = props

  return (
    <div className={classes.teamTitle}>
      <div className={classes.teamColorBar} />
      <div className={classes.teamTitleText}>
        {t(`liveGame.${ blue ? "blueTeam" : "redTeam" }`)}
      </div>
    </div>
  )
})

export default withTranslation(TeamTitle)