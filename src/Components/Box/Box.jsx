import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

const styles = (theme) => ({
  box: {
    backgroundColor: theme.custom.container,
    border: theme.custom.borderContainer,
  }
})

class Box extends Component {
  render() {
    const {
      classes,
      children,
      margin,
      padding,
      width,
      minWidth,
      borderRadius
    } = this.props

    return (
      <div className={classes.box} style={{ margin, padding, width, borderRadius, minWidth }}>
        {children}
      </div>
    );
  }
}

Box.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  margin: PropTypes.string,
  padding: PropTypes.string,
  width: PropTypes.string,
  minWidth: PropTypes.string,
  borderRadius: PropTypes.string
}

Box.defaultProps = {
  margin: '0 auto 0 auto',
  padding: '15px 20px 15px 20px',
  width: '40%',
  minWidth: 'auto',
  borderRadius: '2px'
}

export default withStyles(styles)(Box);