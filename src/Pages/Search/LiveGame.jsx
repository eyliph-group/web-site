import React, { Component, Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { unstable_createResource } from 'react-cache'

import TeamArray from '../../Components/LiveGame/TeamArray'

import Api from '../../js/api/endPoint'
import GameSubInfo from '../../Components/LiveGame/GameSubInfo'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '100%',
    position: 'relative'
  },
})

const summonerLiveGameResource = unstable_createResource(Api.fetchLiveGameDataByID)

class LiveGame extends Component {

  render() {
    const { classes, summonerInfo } = this.props
    const liveGameData = summonerLiveGameResource.read(JSON.stringify({region: summonerInfo.region, id: summonerInfo.id}))

    return (
      <div className={classes.main}>
        { !liveGameData.status &&
          <Fragment>
            <GameSubInfo queueId={liveGameData.gameQueueConfigId} gameStartTime={liveGameData.gameStartTime} />
            <TeamArray
              blue
              participants={liveGameData.participants}
              bannedChampions={liveGameData.bannedChampions}
              region={liveGameData.region}
              queueId={liveGameData.gameQueueConfigId}
              level={summonerInfo.summonerLevel}
            />
            <TeamArray
              red
              participants={liveGameData.participants}
              bannedChampions={liveGameData.bannedChampions}
              region={liveGameData.region}
              queueId={liveGameData.gameQueueConfigId}
              level={summonerInfo.summonerLevel}
            />
          </Fragment>
        }
        { liveGameData.status &&
          <div>
            {JSON.stringify(liveGameData.status)}
          </div>
        }
      </div>
    );
  }
}


LiveGame.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  summonerInfo: PropTypes.instanceOf(Object).isRequired,
}

LiveGame.defaultProps = { }

export default withStyles(styles)(LiveGame);