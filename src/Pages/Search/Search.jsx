import React, { Component, Suspense, Fragment } from 'react';
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { unstable_createResource } from 'react-cache'

import Searchbox from '../../Components/Searchbox/SearchBox'
import Box from '../../Components/Box/Box'
import Header from '../../Components/Header/Header'
import Loading from '../../Components/Utils/Loading'
import LiveGame from  './LiveGame'

import Api from '../../js/api/endPoint'
import { parseQuery } from '../../js/utils'

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '100%',
    position: 'relative'
  },
  body: {
    backgroundColor: theme.custom.bodyBackground,
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    zIndex: 9,
  },
  searchBoxContainer: {
    width: '100%',
    position: 'absolute',
    textAlign: 'center',
    top: '30%',
  }
})

const summonerInfoResource = unstable_createResource(Api.fetchAuthDataByName)

function LiveGameWrapper({ summonerName, region }) {
  return (
    <Fragment>
      { summonerName && region &&
        <Box margin={'20px auto'} padding='0 0 10px 0' width='70%' minWidth='1200px'>
          <LiveGame summonerInfo={summonerInfoResource.read(JSON.stringify({ summonerName, region }))} />
        </Box>
      }
    </Fragment>
  )
}

class SearchPage extends Component {
  state = {
    summonerName: "",
    region: "EUW1",
  }
            // history.push({
            //   pathname: `/profile/${summonerInfo.name}`,
            //   search: `region=${region}`,
            //   state: { summonerInfo: summonerInfo }
            // })

  componentDidMount() {
    const { summonerName, region } = parseQuery(window.location.search)

    if (!summonerName || !region) return
    this.onSearch(region, summonerName)
  }

  onSearch = (_region, _summonerName) => {
    const { summonerName, region } = this.state

    if (region === _region && summonerName.toUpperCase() === _summonerName.toUpperCase()) return
    this.setState({ summonerName: _summonerName, region: _region })
  }

  onChangeTheme = () => {
    const { onChangeTheme } = this.props
    const { summonerName, region } = this.state

    summonerName !== "" && window.history.pushState(null, '', `?summonerName=${summonerName}&region=${region}`)
    onChangeTheme()
  }

  render() {
    const { classes, darkMode } = this.props
    const { summonerName, region } = this.state

    return (
      <div className={classes.main}>
        <Header onChangeTheme={this.onChangeTheme} darkMode={darkMode} />
        <div className={classes.body}>
          <div className={classes.searchBoxContainer}>
            <Box>
              <Searchbox onSearch={this.onSearch} summonerName={summonerName} region={region} />
            </Box>
            <Suspense fallback={<Box margin={'20px auto'}><Loading /></Box>}>
              <LiveGameWrapper summonerName={summonerName} region={region} />
            </Suspense>
          </div>
        </div>
      </div>
    );
  }
}


SearchPage.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  onChangeTheme: PropTypes.func.isRequired,
  darkMode: PropTypes.bool.isRequired,
}

SearchPage.defaultProps = { }

export default withStyles(styles)(withRouter(SearchPage));