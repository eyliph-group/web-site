import React, { Component, Suspense } from 'react';
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'

import Header from '../../Components/Header/Header.jsx'
import Box from '../../Components/Box/Box'

import Region from '../../json/data/region.json'
import Error from '../../json/data/requestError.json'
import { Config } from '../../js/config'
import { parseQuery } from '../../js/utils'

const SummonerInfo = React.lazy(() => import('./SummonerInfo'))

const styles = (theme) => ({
  main: {
    width: '100%',
    height: '100%',
    position: 'relative'
  },
  body: {
    backgroundColor: theme.custom.bodyBackground,
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    zIndex: 9,
    boxSizing: 'border-box',
    padding: '70px 0 0 0',
  },
  profileContainer: {
    margin: '15px 50px 0 50px',
  }
})

class ProfilePage extends Component {
  _isMounted = false
  state = {
    summonerInfo: { profileIconId: -1, name: '', summonerLevel: -1 },
    status: undefined,
    region: {
      platform: "EUW1",
      name: "EUW"
    },
  }

  componentDidMount() {
    this._isMounted = true
    const { location } = this.props
    const summonerInfo = location.state ? location.state.summonerInfo : undefined

    if (summonerInfo === undefined) {
      this.getSummonerInfoByURL()
    } else {
      this.setState({ summonerInfo })
    }
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  fetchSummonerInfo = () => {
    const { location } = this.props
    const { region } = this.state
    const name = location.pathname.substring(9)

    fetch(`${Config.apiEndpoint}/auth/${name}?region=${region.platform}`, {
      method: 'GET'
    })
    .then(res => res.json())
    .then(json => {
      this._isMounted && this.setState({
        status: Error.find(e => json.status && e.status_code === json.status.status_code),
        summonerInfo: json.status ? undefined : json,
      }, () => {
        const { history } = this.props
        const { status } = this.state

        if (status)
          history.push('/')
      })
    })
    .catch(err => this.props.history.push('/'))
  }

  getSummonerInfoByURL = () => {
    const { location } = this.props
    const query = !location.search || location.search === '' ? undefined : parseQuery(location.search)

    if (query && query.region) {
      var region = Region.find(e => query.region.toUpperCase() === e.name || query.region.toUpperCase() === e.platform)
      if (region === undefined) region = Region.find(e => 'EUW' === e.name)
      this.setState({ region }, this.fetchSummonerInfo)
    } else {
      this.fetchSummonerInfo()
    }
  }

  render() {
    const { classes, onChangeTheme } = this.props
    const { summonerInfo } = this.state

    return (
      <div className={classes.main}>
        <Header onChangeTheme={onChangeTheme} />
        <div className={classes.body}>
          <div className={classes.profileContainer}>
            <Box margin='0' borderRadius='0' width='fit-content'>
              <Suspense fallback={<></>}>
                <SummonerInfo profileIcon={summonerInfo.profileIconId} summonerLevel={summonerInfo.summonerLevel} summonerName={summonerInfo.name} />
              </Suspense>
            </Box>
            {JSON.stringify(summonerInfo)}
          </div>
        </div>
      </div>
    );
  }
}

ProfilePage.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  onChangeTheme: PropTypes.func.isRequired
}

ProfilePage.defaultProps = { }

export default withStyles(styles)(withRouter(ProfilePage));