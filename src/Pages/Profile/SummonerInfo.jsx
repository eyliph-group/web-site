import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

import SummonerLevel from '../../Components/Profile/SummonerLevel'
import FileCopyIcon from '@material-ui/icons/FileCopy'

import { Config } from '../../js/config'

const styles = (theme) => ({
  main: {

  },
  container: {
    padding: '10px 30px',
  },
  imgContainer: {
    width: '150px',
    height: '150px',
    borderRadius: '50%',
    border: '2px solid #eabd70',
    WebkitBoxShadow: 'inset 0px 0px 20px 7px #101010, 0px 0px 10px 0px #101010',
    MozBoxShadow: 'inset 0px 0px 20px 7px #101010, 0px 0px 10px 0px #101010',
    boxShadow: 'inset 0px 0px 20px 7px #101010, 0px 0px 10px 0px #101010',
  },
  profileImg: {
    maxWidth: '150px',
    borderRadius: '50%',
  },
  nameContainer: {
    display: 'flex',
  },
  nameText: {
    marginLeft: 'auto',
    textAlign: 'center',
    fontWeight: 700,
    color: theme.custom.textColor,
  },
  copyButton: {
    marginLeft: '10px',
    marginRight: 'auto',
    border: 'none',
    outline: 'none',
    padding: 0,
    height: 'fit-content',
    backgroundColor: 'transparent',
    color: theme.custom.disableTextColor,
    '&:hover': {
      color: theme.custom.textColor,
      cursor: 'pointer',
    }
  },
  copyIcon: {
    width: 20,
  }
})

class SummonerInfo extends Component {
  copyDivToClipboard = (id) => {
    const range = document.createRange()
    range.selectNode(document.getElementById(id))
    window.getSelection().removeAllRanges()
    window.getSelection().addRange(range)
    document.execCommand("copy")
    window.getSelection().removeAllRanges()
  }

  render() {
    const {
      classes,
      profileIcon,
      summonerLevel,
      summonerName
    } = this.props

    return (
      <div className={classes.main}>
        <div className={classes.container}>
          <div
            className={classes.imgContainer}
            style={{
              background: profileIcon !== -1
                ? `center / contain url(${Config.ddragonEndpoint}/${Config.ddragonApiVersion}/img/profileicon/${profileIcon}.png), black`
                : 'black'
            }}
          />
          <SummonerLevel summonerLevel={summonerLevel} />
        </div>
        <div className={classes.nameContainer}>
          <div id='summNameDiv' className={classes.nameText}>
              {summonerName}
          </div>
          <button className={classes.copyButton} onClick={() => this.copyDivToClipboard('summNameDiv')}>
            <FileCopyIcon className={classes.copyIcon} />
          </button>
        </div>
      </div>
    );
  }
}

SummonerInfo.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
  profileIcon: PropTypes.number.isRequired,
  summonerLevel: PropTypes.number.isRequired,
  summonerName: PropTypes.string.isRequired,
}

SummonerInfo.defaultProps = { }

export default withStyles(styles)(SummonerInfo);