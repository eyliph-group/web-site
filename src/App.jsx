import React, { Component, Suspense } from 'react';
import { BrowserRouter, Route, Switch, withRouter } from 'react-router-dom'
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { setTranslations, setDefaultLanguage } from 'react-multi-lang'

import fr from './json/lang/fr.json'
import en from './json/lang/en.json'

import { light, dark } from './js/theme'

const Search = React.lazy(() => import('./Pages/Search/Search.jsx'))
const Profile = React.lazy(() => import('./Pages/Profile/Profile.jsx'))

const lightTheme = createMuiTheme({
  custom: {
    ...light,
  }
});

const darkTheme = createMuiTheme({
  custom: {
    ...dark,
  }
});

setTranslations({fr, en})
setDefaultLanguage('fr')

class RedirectHome extends Component {
  componentDidMount() {
    const { history } = this.props

    history.push('/')
  }
  render() { return (<></>) }
}

class App extends Component {
  state = {
    darkMode: true
  }

  componentDidMount() {
    const darkMode = window.localStorage.getItem('darkMode')

    if (!darkMode || darkMode === 'true')
      this.setState({ darkMode: true })
    else
      this.setState({ darkMode: false })
  }

  onChangeTheme = () => {
    this.setState((state) => ({ darkMode: !state.darkMode }), () => {
      const { darkMode } = this.state

      window.localStorage.setItem('darkMode', darkMode ? 'true' : 'false')
    })
  }

  render() {
    const { darkMode } = this.state

    return (
      <Suspense fallback={<div></div>}>
        <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/profile/:summ" component={() => <Profile onChangeTheme={this.onChangeTheme} darkMode={darkMode} />} />
              <Route exact path="/Lorion/?query" component={() => <Search onChangeTheme={this.onChangeTheme} darkMode={darkMode} />} />
              <Route exact path="/Lorion" component={() => <Search onChangeTheme={this.onChangeTheme} darkMode={darkMode} />} />
              <Route exact path="/?query" component={() => <Search onChangeTheme={this.onChangeTheme} darkMode={darkMode} />} />
              <Route exact path="/" component={() => <Search onChangeTheme={this.onChangeTheme} darkMode={darkMode} />} />
              <Route exact path="/*" component={withRouter(RedirectHome)} />
            </Switch>
          </BrowserRouter>
        </ThemeProvider>
      </Suspense>
    );
  }
}

// Flag Icon credit : <div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

export default App;
