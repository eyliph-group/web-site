module.exports = {
    light: {
        headerBackground: "#f0f0f0",
        headerButton: "black",
        bodyBackground: "#e0e0e0",
        container: "#d0d0d0",
        borderContainer: "1px solid #c8c8c8",
        textColor: 'white',
        disableTextColor: '#ffffff50',
        search: {
            inputBorder: '1px solid #606060',
            blueInputBorder: '1px solid #07adc0',
            inputBackground: '#c0c0c0',
            textColor: '#151515',
            button: {
                default: '#07adc0',
                disabled: '#069cad',
                text: 'white',
            }
        },
        profile: {

        },
        liveGame: {
            blueTeam: '#1f8ecd',
            redTeam: '#ee5a52',
            subColor: '#ACACAC',
            triColor: '#555555',
            green: '#178c90',
            orange: '#dc8c07',
            textColor: '#232323',
        }
    },
    dark: {
        headerBackground: "#151515",
        headerButton: "white",
        bodyBackground: "#202020",
        container: "#282828",
        borderContainer: "1px solid #404040",
        textColor: '#ffffffbb',
        disableTextColor: '#ffffff50',
        search: {
            inputBorder: '1px solid #171717',
            blueInputBorder: '1px solid #07adc0',
            inputBackground: '#454545',
            textColor: '#858585',
            button: {
                default: '#07adc0',
                disabled: '#069cad',
                text: 'white',
            }
        },
        profile: {

        },
        liveGame: {
            blueTeam: '#1f8ecd',
            redTeam: '#ee5a52',
            subColor: '#555555',
            triColor: '#acacac',
            green: '#37acb0',
            orange: '#ec9c17',
            textColor: '#eeeeee',
        }
    }
}