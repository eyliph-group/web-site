const queueId = require('../json/data/Riot_queueId.json')

const constants = {
    apiEndpoint: '',
    ddragonEndpoint: 'https://ddragon.leagueoflegends.com/cdn',
    ddragonApiVersion: '',
    runesReforged: {},
    championsInfo: {},
    summSpells: {},
    queueId,
}

const env = process.env.REACT_APP_ENV

if (env === 'dev') {
    constants.apiEndpoint = 'http://localhost:8080'
} else if (env === 'stage') {
    constants.apiEndpoint = 'https://lorion-api.herokuapp.com'
} else if (env === 'prod') {
    constants.apiEndpoint = 'https://lorion-api.herokuapp.com'
}

const getDDragonApiVersion = async() => {
    const version = window.localStorage.getItem('ddragonApiVersion')
    if (!version) {
        await fetch('https://ddragon.leagueoflegends.com/api/versions.json')
            .then(res => res.json())
            .then(json => {
                constants.ddragonApiVersion = json[0]
                window.localStorage.setItem('ddragonApiVersion', constants.ddragonApiVersion)
            })
    } else {
        constants.ddragonApiVersion = version
    }
}

const getRunesReforged = async() => {
    await fetch(`${constants.ddragonEndpoint}/${constants.ddragonApiVersion}/data/fr_FR/runesReforged.json`)
        .then(res => res.json())
        .then(json => { constants.runesReforged = json })
}

const getChampions = async() => {
    await fetch(`${constants.ddragonEndpoint}/${constants.ddragonApiVersion}/data/fr_FR/champion.json`)
        .then(res => res.json())
        .then(json => { constants.championsInfo = json })
}

const getSummonerSpells = async() => {
    await fetch(`${constants.ddragonEndpoint}/${constants.ddragonApiVersion}/data/fr_FR/summoner.json`)
        .then(res => res.json())
        .then(json => { constants.summSpells = json })
}

getDDragonApiVersion()
getRunesReforged()
getChampions()
getSummonerSpells()

export { constants as Config }