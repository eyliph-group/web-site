import Unranked from '../Assets/Rank/Emblem_Unranked.png'
import Iron from '../Assets/Rank/Emblem_Iron.png'
import Bronze from '../Assets/Rank/Emblem_Bronze.png'
import Silver from '../Assets/Rank/Emblem_Silver.png'
import Gold from '../Assets/Rank/Emblem_Gold.png'
import Platinum from '../Assets/Rank/Emblem_Platinum.png'
import Diamond from '../Assets/Rank/Emblem_Diamond.png'
import Master from '../Assets/Rank/Emblem_Master.png'
import Grandmaster from '../Assets/Rank/Emblem_Grandmaster.png'
import Challenger from '../Assets/Rank/Emblem_Challenger.png'

const rank = [{
        id: "UNRANKED",
        component: Unranked,
    },
    {
        id: "IRON",
        component: Iron,
    },
    {
        id: "BRONZE",
        component: Bronze,
    },
    {
        id: "SILVER",
        component: Silver,
    },
    {
        id: "GOLD",
        component: Gold,
    },
    {
        id: "PLATINUM",
        component: Platinum,
    },
    {
        id: "DIAMOND",
        component: Diamond,
    },
    {
        id: "MASTER",
        component: Master,
    },
    {
        id: "GRANDMASTER",
        component: Grandmaster,
    },
    {
        id: "CHALLENGER",
        component: Challenger,
    },
]

export default {
    getRankComponentByID: id => rank.find(e => e.id === id).component,
}