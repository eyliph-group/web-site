import { Config } from '../config'

const Api = {
    fetchAuthDataByName: (obj) => {
        const { region, summonerName } = JSON.parse(obj)
        const promise = fetch(`${Config.apiEndpoint}/auth/${summonerName}?region=${region}`, {
                method: 'GET'
            })
            .then(res => res.json())
            .then(json => ({...json, region }))

        return promise
    },
    fetchRankDataByID: (obj) => {
        const { region, id } = JSON.parse(obj)
        const promise = fetch(`${Config.apiEndpoint}/rank/${id}?region=${region}`, {
                method: 'GET'
            })
            .then(res => res.json())
            .then(json => ({ data: [...json], region }))
        return promise
    },
    fetchLiveGameDataByID: (obj) => {
        const { region, id } = JSON.parse(obj)
        const promise = fetch(`${Config.apiEndpoint}/livegame/${id}?region=${region}`, {
                method: 'GET'
            })
            .then(res => res.json())
            .then(json => ({...json, region }))
        return promise
    }
}

export default Api